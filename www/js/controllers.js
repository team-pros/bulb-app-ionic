angular.module('app.controllers', [])
  
.controller('devicesCtrl', function($scope, $http, DeviceStorage) {
  $scope.devices = DeviceStorage.getAllDevices();

  $scope.addDevice = function() {
    DeviceStorage.addDevice();
  }

  $scope.init = function() {
    DeviceStorage.init();
  }
})





// TODO
// OK Zapamiętanie znalezionego urządzenia na liście zapamiętanych urządzeń  
// OK Możliwość dodania urządzenia tylko jak już go nie ma na liście zapamiętanych (po czym poznawać - IP?)
//    Czy potrzebne jest id?  Może tą funkcję spelni IP?
//    Czy można skonfigurować ciąg znaków jaki zwraca urzdzenie jak szukamy po mdns?
//    configURL?

.controller('scanCtrl', function($scope, DeviceStorage) {
  $scope.foundDevices = [];
  $scope.progress = 0;
  $scope.data = {password:""};   // samo $scope.password nie działa: html nie aktualizuje js
  $scope.scanLabel = "Scan";


  $scope.search = function() {

    if ($scope.scanLabel == "Scan") {
      $scope.progress = 0;
      $scope.foundDevices.length = 0;

      // Callback on success (found device / next tick / finish)
      function onSuccess(result) {
        console.log('SmartconfigResult: ' + result.eventType + ' ' + result.progress + ' ' + result.ip + ' ' + result.name);
        if (result.eventType == SmartconfigResult.DEVICE_FOUND) {
          $scope.foundDevices.push({id:result.name,ip:result.ip});
          $scope.$apply();
        }
        if (result.eventType == SmartconfigResult.PROGRESS) {
          $scope.progress = result.progress;
          $scope.$apply();
        }
        if (result.eventType == SmartconfigResult.SEARCH_FINISHED) {
          $scope.scanLabel = "Scan";
          $scope.progress = 100;
          $scope.$apply();
        }
      };

      // Callback in case of failure
      function onError(error) {
        console.log('Smartconfig search error' + error);
      };

      
      // Is this browser or mobile?
      if (navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry|IEMobile)/)) {
        // Invoke smartconfig
        $scope.scanLabel = "Stop";
        console.log('To jest urzadzenie - rozpoczynam poszukiwania');
        SmartconfigPlugin.search(onSuccess, onError,$scope.data.password);
      } else {
        console.log('To jest przegladarka - na sztywno trzy urzadzenia');
        $scope.foundDevices.push({id:"bulb_0010",ip:"192.168.1.1"});
        $scope.foundDevices.push({id:"bulb_0011",ip:"192.168.1.2"});
        $scope.foundDevices.push({id:"dim_0012",ip:"192.168.1.3"});
      }

     
     

    }

    else if ($scope.scanLabel == "Stop") {
      $scope.scanLabel = "Stopping";
      SmartconfigPlugin.stopSearching();
    }  

  };


  $scope.add = function(device) {
    console.log('add' + device.id + ' ' + device.ip);
    //DeviceStorage.addDevice({id:4, name:device.name, type:"bulb", configURL:"http://192.168.2.1:1234", ip:device.ip, pic:"bulb.jpg"});
    //DeviceStorage.addDevice({id:4, name:device.name, type:"bulb", configURL:"http://" + device.ip, ip: device.ip, pic:"bulb.jpg"});
    if (device.id.indexOf('dim') >= 0) {
      DeviceStorage.addDevice({id:device.id, name:device.id, type:"dimmer", configURL:"http://" + device.ip, ip: device.ip, pic:"dimmer.jpg"});
    } 
    // bulb - default type of device 
    else {
      DeviceStorage.addDevice({id:device.id, name:device.id, type:"bulb", configURL:"http://" + device.ip, ip: device.ip, pic:"bulb.jpg"});
    }
    

  };


  // checks if device with this id already exists on list of devices
  $scope.alreadySaved = function(device) {
    var founded = DeviceStorage.isID(device.id);
    return founded;
  };
  


})
   
.controller('settingsCtrl', function($scope) {
	var deploy = new Ionic.Deploy();
  
  // Update app code with new release from Ionic Deploy
  $scope.doUpdate = function() {
    deploy.update().then(function(res) {
      console.log('Ionic Deploy: Update Success! ', res);
    }, function(err) {
      console.log('Ionic Deploy: Update error! ', err);
    }, function(prog) {
      console.log('Ionic Deploy: Progress... ', prog);
    });
  };

  // Check Ionic Deploy for new code
  $scope.checkForUpdates = function() {
    console.log('Ionic Deploy: Checking for updates');
    deploy.check().then(function(hasUpdate) {
      console.log('Ionic Deploy: Update available: ' + hasUpdate);
      $scope.hasUpdate = hasUpdate;
    }, function(err) {
      console.error('Ionic Deploy: Unable to check for updates', err);
    });
  }

})
      
.controller('deviceDetailsCtrl', function($scope, $stateParams, $http, DeviceStorage) {

  $scope.device = DeviceStorage.getDeviceByIndex($stateParams.deviceIndex);

  var token = '__SL_P_ROS=ok';
  var req = {
    method: 'POST',
    url: '',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    data: token
  }

	$scope.switch = function() {
		var basicURL = $scope.device.configURL+"/bulb/api/v1/"; //http://localhost:4000/bulb/api/v1/status
		if (this.bulbState) {
			bulbURL = basicURL+"on";
		} else {
			bulbURL = basicURL+"off";
		}
    req.url = bulbURL;

    console.log('Sending request to ' + bulbURL); 
		$http(req);
	};

  $scope.$watch('device.configURL',function (oldValue, newValue) {
     console.log('Storing new values of devices');
     //DeviceStorage.storeTheList();
  });

})
 
