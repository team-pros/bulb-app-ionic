angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    
  

      .state('tabsController.devices', {
    url: '/device-list',
    views: {
      'tab1': {
        templateUrl: 'templates/devices.html',
        controller: 'devicesCtrl'
      }
    }
  })

  .state('tabsController.scan', {
    url: '/scan',
    views: {
      'tab2': {
        templateUrl: 'templates/scan.html',
        controller: 'scanCtrl'
      }
    }
  })

  .state('tabsController.settings', {
    url: '/settings',
    views: {
      'tab3': {
        templateUrl: 'templates/settings.html',
        controller: 'settingsCtrl'
      }
    }
  })

  .state('tabsController', {
    url: '/app',
    templateUrl: 'templates/tabsController.html',
    abstract:true
  })

  .state('tabsController.deviceDetails', {
    url: '/device-details/:deviceIndex',
    views: {
      'tab1': {
        templateUrl: 'templates/deviceDetails.html',
        controller: 'deviceDetailsCtrl'
      }
    }
  })

$urlRouterProvider.otherwise('/app/device-list')

  

});
