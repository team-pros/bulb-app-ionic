angular.module('app.services', [])

.factory('BlankFactory', [function(){

}])

.service('BlankService', [function(){

}])

.service('DeviceStorage', function($filter, $localStorage) {

	var _listOfDevices;
	
	var init = function() {
		console.log("DeviceStorage init");
		console.log($localStorage.devices);

		if(typeof $localStorage.devices === 'undefined') {
			$localStorage.devices = [
				//{"id": 1, "name": "Kitchen Bulb", "type": "bulb", "configURL": "http://192.168.2.1:1234", "ip": "192.168.2.1", "pic":"bulb.jpg" },
				//{"id": 2, "name": "Over the Table Bulb", "type": "bulb", "configURL": "http://192.168.1.147:4000", "ip": "192.168.1.147", "pic": "bulb.jpg"},
				//{"id": 3, "name": "Living Room Dimmer", "type": "dimmer", "configURL": "http://192.168.2.4:2345", "ip": "192.168.2.4", "pic": "dimmer.jpg"}
				{"id": "bulb_0001", "name": "Kitchen Bulb", "type": "bulb", "configURL": "http://192.168.2.1:1234", "ip": "192.168.2.1", "pic":"bulb.jpg" },
				{"id": "bulb_0002", "name": "Over the Table Bulb", "type": "bulb", "configURL": "http://192.168.1.147:4000", "ip": "192.168.1.147", "pic": "bulb.jpg"},
				{"id": "dim_0001", "name": "Living Room Dimmer", "type": "dimmer", "configURL": "http://192.168.2.4:2345", "ip": "192.168.2.4", "pic": "dimmer.jpg"}
			];
		};


	};

	this.addDevice = function(device) {
		_listOfDevices.push(device);
		this.storeTheList();
	};

	this.removeDevice = function(index) {
		_listOfDevices.splice(index);
		this.storeTheList();
	};

	this.getDeviceById = function(id) {
		return $filter('filter')(_listOfDevices, {id : id})[0];
	};

	this.getDeviceByIndex = function(index) {
		return _listOfDevices[index];
	}

	this.getAllDevices = function() {
		init();
		_listOfDevices = $localStorage.devices;
		console.log("Device count = " + _listOfDevices.length);
		return _listOfDevices;
	};

	this.isIP = function(search_ip) {
		var found = $filter('filter')(_listOfDevices, {ip: search_ip}, true);
		if (found.length) {
			return true;
		} else {
			return false;
		}
	};

	this.isID = function(search_id) {
		var found = $filter('filter')(_listOfDevices, {id: search_id}, true);
		if (found.length) {
			return true;
		} else {
			return false;
		}
	};


	this.storeTheList = function() {
		//$localStorage['devices'] = JSON.stringify(_listOfDevices);
		$localStorage.devices = _listOfDevices;
	};
});

